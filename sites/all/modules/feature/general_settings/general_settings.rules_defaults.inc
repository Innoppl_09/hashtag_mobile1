<?php
/**
 * @file
 * general_settings.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function general_settings_default_rules_configuration() {
  $items = array();
  $items['rules_add_category_in_hashtags'] = entity_import('rules_config', '{ "rules_add_category_in_hashtags" : {
      "LABEL" : "Add category in hashtags",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--hash_tag_news" : { "bundle" : "hash_tag_news" },
        "node_update--hash_tag_news" : { "bundle" : "hash_tag_news" }
      },
      "IF" : [ { "NOT data_is_empty" : { "data" : [ "node:field-news-category" ] } } ],
      "DO" : [
        { "list_add" : {
            "list" : [ "node:field-news-hashtags" ],
            "item" : [ "node:field-news-category" ],
            "pos" : "start"
          }
        }
      ]
    }
  }');
  $items['rules_redirect_unonymous_user_upon_aubmit_hashtag'] = entity_import('rules_config', '{ "rules_redirect_unonymous_user_upon_aubmit_hashtag" : {
      "LABEL" : "Redirect anonymous user upon Submit a HashTag",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "text_matches" : {
            "text" : [ "site:current-page:path" ],
            "match" : "node\\/add\\/hash-tag-news"
          }
        },
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "anonymous-user", "destination" : "1" } } ]
    }
  }');
  return $items;
}
