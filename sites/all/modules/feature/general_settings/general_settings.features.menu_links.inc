<?php
/**
 * @file
 * general_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function general_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_business:<void>.
  $menu_links['main-menu_business:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => 'Business',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_business:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_entertainment:<void>.
  $menu_links['main-menu_entertainment:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Entertainment',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_entertainment:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_more-1:<void>.
  $menu_links['main-menu_more-1:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More 1',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more-1:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_more:<void>',
  );
  // Exported menu link: main-menu_more-2:<void>.
  $menu_links['main-menu_more-2:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More 2',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more-2:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_more:<void>',
  );
  // Exported menu link: main-menu_more:<void>.
  $menu_links['main-menu_more:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#More',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_more:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_politics:<void>.
  $menu_links['main-menu_politics:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Politics',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_politics:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_sports:<void>.
  $menu_links['main-menu_sports:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Sports',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_sports:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_technology:<void>.
  $menu_links['main-menu_technology:<void>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<void>',
    'router_path' => '<void>',
    'link_title' => '#Technology',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'unaltered_hidden' => 0,
      'external' => TRUE,
      'identifier' => 'main-menu_technology:<void>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('#Entertainment');
  t('#More');
  t('#More 1');
  t('#More 2');
  t('#Politics');
  t('#Sports');
  t('#Technology');
  t('Business');

  return $menu_links;
}
