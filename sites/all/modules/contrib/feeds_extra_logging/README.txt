CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

A module to log more things during a Feeds import.

REQUIREMENTS
------------

[Feeds](https://www.drupal.org/project/feeds), version 7.x-2.0-beta2 or higher,
with the latest patch from https://www.drupal.org/node/2825162 applied.

INSTALLATION
------------

1. Download, install, and enable the fields_extra_logging project.

   See https://drupal.org/node/895232 for further information.

   The module will start working automatically: no further action is required.

CONFIGURATION
-------------

This module has no configuration.

MAINTAINERS
-----------

Concept and coding by mparker17. Sponsored by Digtial Echidna
(https://www.echidna.ca/).
