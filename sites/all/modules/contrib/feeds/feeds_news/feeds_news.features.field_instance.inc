<?php
/**
 * @file
 * feeds_news.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function feeds_news_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-feed-field_hashtag_account_category'.
  $field_instances['node-feed-field_hashtag_account_category'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_category',
    'label' => 'Hashtag Account Category',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'autocomplete_path' => 'taxonomy/autocomplete',
        'size' => 60,
      ),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-feed-field_hashtag_account_hash'.
  $field_instances['node-feed-field_hashtag_account_hash'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_hash',
    'label' => 'Hashtag Account hash',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-feed-field_hashtag_account_name'.
  $field_instances['node-feed-field_hashtag_account_name'] = array(
    'bundle' => 'feed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_hashtag_account_name',
    'label' => 'Hashtag Account Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
        'soft_length_limit' => '',
        'soft_length_minimum' => '',
        'soft_length_style_select' => 0,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-category-field_tag_icon'.
  $field_instances['taxonomy_term-category-field_tag_icon'] = array(
    'bundle' => 'category',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_tag_icon',
    'label' => 'Tag Icon',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Hashtag Account Category');
  t('Hashtag Account Name');
  t('Hashtag Account hash');
  t('Tag Icon');

  return $field_instances;
}
