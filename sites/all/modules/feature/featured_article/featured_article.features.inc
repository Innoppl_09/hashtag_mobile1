<?php
/**
 * @file
 * featured_article.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function featured_article_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function featured_article_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function featured_article_image_default_styles() {
  $styles = array();

  // Exported image style: article_image__590x300_.
  $styles['article_image__590x300_'] = array(
    'label' => 'Article Image (590x300)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 590,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function featured_article_node_info() {
  $items = array(
    'add_news_article' => array(
      'name' => t('#News Featured Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
