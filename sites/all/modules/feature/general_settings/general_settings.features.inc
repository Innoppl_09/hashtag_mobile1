<?php
/**
 * @file
 * general_settings.features.inc
 */

/**
 * Implements hook_fontyourface_features_default_font().
 */
function general_settings_fontyourface_features_default_font() {
  return array(
    'Montserrat normal normal' => array(
      'name' => 'Montserrat normal normal',
      'enabled' => 1,
      'url' => 'http://localhost/#5a1a9a1c3c085c36860332bd3119a3c4',
      'provider' => 'local_fonts',
      'css_selector' => '<none>',
      'css_family' => 'Montserrat',
      'css_style' => 'normal',
      'css_weight' => 'normal',
      'css_fallbacks' => '',
      'foundry' => '',
      'foundry_url' => '',
      'license' => '',
      'license_url' => '',
      'designer' => '',
      'designer_url' => '',
      'metadata' => 'a:2:{s:4:"path";s:73:"public://fontyourface/local_fonts/Montserrat-normal-normal/stylesheet.css";s:8:"font_uri";a:4:{s:3:"eot";s:81:"public://fontyourface/local_fonts/Montserrat-normal-normal/Montserrat-Regular.eot";s:8:"truetype";s:81:"public://fontyourface/local_fonts/Montserrat-normal-normal/Montserrat-Regular.ttf";s:4:"woff";s:82:"public://fontyourface/local_fonts/Montserrat-normal-normal/Montserrat-Regular.woff";s:3:"svg";s:81:"public://fontyourface/local_fonts/Montserrat-normal-normal/Montserrat-Regular.svg";}}',
    ),
  );
}

/**
 * Implements hook_default_mailchimp_signup().
 */
function general_settings_default_mailchimp_signup() {
  $items = array();
  $items['data_capture'] = entity_import('mailchimp_signup', '{
    "name" : "data_capture",
    "mc_lists" : { "2904fcd1f0" : "2904fcd1f0" },
    "mode" : "1",
    "title" : "Data Capture",
    "settings" : {
      "path" : "",
      "submit_button" : "Signup",
      "confirmation_message" : "You have been successfully subscribed.",
      "destination" : "",
      "mergefields" : { "EMAIL" : {"tag":"EMAIL","name":"Email Address","type":"email","required":true,"default_value":"","public":true,"display_order":1,"options":{"size":25}} },
      "description" : "",
      "doublein" : 1,
      "include_interest_groups" : 0
    },
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function general_settings_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
