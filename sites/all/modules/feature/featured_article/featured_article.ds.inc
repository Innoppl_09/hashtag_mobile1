<?php
/**
 * @file
 * featured_article.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function featured_article_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|add_news_article|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'add_news_article';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'ds_post_date_long',
    ),
  );
  $export['node|add_news_article|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function featured_article_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|add_news_article|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'add_news_article';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_12';
  $ds_layout->settings = array(
    'regions' => array(
      'central' => array(
        0 => 'field_news_category',
        1 => 'field_news_hashtags',
        2 => 'post_date',
        3 => 'field_featured_article_image',
        4 => 'body',
        5 => 'field_external_content',
        6 => 'disqus',
      ),
    ),
    'fields' => array(
      'field_news_category' => 'central',
      'field_news_hashtags' => 'central',
      'post_date' => 'central',
      'field_featured_article_image' => 'central',
      'body' => 'central',
      'field_external_content' => 'central',
      'disqus' => 'central',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'central' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|add_news_article|default'] = $ds_layout;

  return $export;
}
