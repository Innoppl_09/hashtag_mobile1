<?php
/**
 * @file
 * feeds_news.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function feeds_news_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-blank_source_5-default_value';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'Blank source 5';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => 'Twitter',
    'only_if_empty' => 1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set value or default value';
  $export['feed-blank_source_5-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-caption-php';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'caption';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => '$string = preg_replace(\'/\\b(https?):\\/\\/[-A-Z0-9+&@#\\/%?=~_|$!:,.;]*[A-Z0-9+&@#\\/%=~_|$]/i\', \'\', $field);
return $string;',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';
  $export['feed-caption-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-created_at-timetodate';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'created_at';
  $feeds_tamper->plugin_id = 'timetodate';
  $feeds_tamper->settings = array(
    'date_format' => 'Y-m-d H:i:s',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Unix timestamp to Date';
  $export['feed-created_at-timetodate'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-hashtags-explode';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'hashtags';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 6;
  $feeds_tamper->description = 'Explode';
  $export['feed-hashtags-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-hashtags-find_replace_regex';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'hashtags';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '([,])',
    'replace' => ',#',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 5;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['feed-hashtags-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-hashtags-implode';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'hashtags';
  $feeds_tamper->plugin_id = 'implode';
  $feeds_tamper->settings = array(
    'glue' => ',',
    'real_glue' => ',',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Implode';
  $export['feed-hashtags-implode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-hashtags-php';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'hashtags';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'if(empty($field)) {
return \'\';
} else {
return \'#\' . $field;
}',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Execute php code';
  $export['feed-hashtags-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'feed-media_list_type_link_url-required';
  $feeds_tamper->importer = 'feed';
  $feeds_tamper->source = 'media_list_type_link_url';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 0,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Required field';
  $export['feed-media_list_type_link_url-required'] = $feeds_tamper;

  return $export;
}
