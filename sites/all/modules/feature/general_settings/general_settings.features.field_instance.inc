<?php
/**
 * @file
 * general_settings.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function general_settings_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page-body'.
  $field_instances['node-page-body'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
          'masonry_stamp_selector' => '',
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');

  return $field_instances;
}
