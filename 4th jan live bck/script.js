(function($) {
  $(document).ready(function() {
    /* $('<div class="slider_count"></div>').insertBefore(".slider_image_wrapper"); 
    var totalItems = $('.node-type-add-news-article .owl-item').length;
    var currentIndex = $('div.active').index() + 1;
    $(".slider_count").html( currentIndex + "/" + totalItems); */
    $(".node-type-add-news-article .owl-next").click(function(){
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index() + 1;
      $(".slider_count").html( currentIndex + "/" + totalItems);
      googletag.pubads().refresh();
      if ($(".owl-item.active").prev().find('.right_side_wrapper .slider_external_link a').length > 0) {
        url = $(".owl-item.active").prev().find('.right_side_wrapper .slider_external_link a').attr('href');
        window.open(url, "_blank");
      }
    });
    $(".node-type-add-news-article .owl-prev").click(function(){
      var totalItems = $('.node-type-add-news-article .owl-item').length;
      var currentIndex = $('div.active').index() + 1;
        $(".slider_count").html( currentIndex + "/" + totalItems);
        googletag.pubads().refresh();
    }); 

    $(".view-hashtags-news-listing-mobile").each(function(){
        $(this).find(".news-viewnews-more").insertAfter($(this).find(".news-addtoany"));
    });
    admin_flag = Drupal.settings.custom_role_set;
    if (screen.width <= 768) {
      if(document.location.host == 'thehashtag.news' || document.location.host == 'www.thehashtag.news' ){
        document.location = "https://m.thehashtag.news" + document.location.pathname;
      }
    }
    if (screen.width > 768) {
      var pathName = document.location.pathname;
      if(document.location.host == 'm.thehashtag.news' && pathName != '/user' && pathName.search('/node') == -1 && pathName.search('/users') == -1 && !admin_flag){
        document.location = "https://www.thehashtag.news" + document.location.pathname;
      }
    }
  });
  $(document).mouseup(function(e) {
    // console.log('mouseup');
  });
  $(window).load(function(e) {
    $('.owl-carousel').each(function(e){
      $(this).find('.owl-wrapper-outer:first').css('min-height', '300px');
    });
    if($('body.node-type-add-news-article').length){
      var Inarticle = document.createElement('script');
      Inarticle.type = 'text/javascript';
      Inarticle.async = true;
      Inarticle.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle1');});";
      $('#Inarticle1').append(Inarticle);
      var rcjsload_7606ad = document.createElement('script');
      rcjsload_7606ad.type = 'text/javascript';
      rcjsload_7606ad.async = true;
      rcjsload_7606ad.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83842&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_7606ad"); rcds.appendChild(rcel);})();';
      $('#rcjsload_7606ad').each(function(e){
        $(this).append(rcjsload_7606ad);
      });
    }
    $('.view-display-id-mobile_articles a.view-more-link').click(function(e) {
      var Inarticle1 = document.createElement('script');
      Inarticle1.type = 'text/javascript';
      Inarticle1.async = true;
      Inarticle1.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle1');});";
      $('#Inarticle1').append(Inarticle1);
      var rcjsload_7606ad = document.createElement('script');
      rcjsload_7606ad.type = 'text/javascript';
      rcjsload_7606ad.async = true;
      rcjsload_7606ad.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83842&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_7606ad"); rcds.appendChild(rcel);})();';
      $('#rcjsload_7606ad').each(function(e){
        $(this).append(rcjsload_7606ad);
      });
    });
    $('.view-display-id-m_politics_articles a.view-more-link').click(function(e) {
      var Inarticle2 = document.createElement('script');
      Inarticle2.type = 'text/javascript';
      Inarticle2.async = true;
      Inarticle2.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle2');});";
      $('#Inarticle2').append(Inarticle2);
      var rcjsload_1a0ad2 = document.createElement('script');
      rcjsload_1a0ad2.type = 'text/javascript';
      rcjsload_1a0ad2.async = true;
      rcjsload_1a0ad2.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=83512&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_1a0ad2"); rcds.appendChild(rcel);})();';
      $('.view-display-id-m_politics_articles').find('#rcjsload_1a0ad2').append(rcjsload_1a0ad2);
    });
    $('.view-display-id-m_sports_articles a.view-more-link').click(function(e) {
      var Inarticle3 = document.createElement('script');
      Inarticle3.type = 'text/javascript';
      Inarticle3.async = true;
      Inarticle3.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle3');});";
      $('#Inarticle3').append(Inarticle3);
      var rcjsload_4b1308 = document.createElement('script');
      rcjsload_4b1308.type = 'text/javascript';
      rcjsload_4b1308.async = true;
      rcjsload_4b1308.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=84227&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_4b1308"); rcds.appendChild(rcel);})();';
      $('.view-display-id-m_sports_articles').find('#rcjsload_4b1308').append(rcjsload_4b1308);
    });
    $('.view-display-id-m_business_articles a.view-more-link').click(function(e) {
      var Inarticle4 = document.createElement('script');
      Inarticle4.type = 'text/javascript';
      Inarticle4.async = true;
      Inarticle4.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle4');});";
      $('#Inarticle4').append(Inarticle4);
      var rcjsload_7d0099 = document.createElement('script');
      rcjsload_7d0099.type = 'text/javascript';
      rcjsload_7d0099.async = true;
      rcjsload_7d0099.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=84230&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_7d0099"); rcds.appendChild(rcel);})();';
      $('.view-display-id-m_business_articles').find('#rcjsload_7d0099').append(rcjsload_7d0099);
    });
    $('.view-display-id-m_editorial_articles a.view-more-link').click(function(e) {
      var Inarticle5 = document.createElement('script');
      Inarticle5.type = 'text/javascript';
      Inarticle5.async = true;
      Inarticle5.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle5');});";
      $('#Inarticle5').append(Inarticle5);
      var rcjsload_be5671 = document.createElement('script');
      rcjsload_be5671.type = 'text/javascript';
      rcjsload_be5671.async = true;
      rcjsload_be5671.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=84231&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_be5671"); rcds.appendChild(rcel);})();';
      $('.view-display-id-m_editorial_articles').find('#rcjsload_be5671').append(rcjsload_be5671);
    });
    $('.view-display-id-m_entertainment_articles a.view-more-link').click(function(e) {
      var Inarticle6 = document.createElement('script');
      Inarticle6.type = 'text/javascript';
      Inarticle6.async = true;
      Inarticle6.innerHTML="googletag.cmd.push(function(){googletag.display('Inarticle6');});";
      $('#Inarticle6').append(Inarticle6);
      var rcjsload_5ba490 = document.createElement('script');
      rcjsload_5ba490.type = 'text/javascript';
      rcjsload_5ba490.async = true;
      rcjsload_5ba490.innerHTML='(function() {var referer="";try{if(referer=document.referrer,"undefined"==typeof referer)throw"undefined"}catch(exception){referer=document.location.href,(""==referer||"undefined"==typeof referer)&&(referer=document.URL)}referer=referer.substr(0,700);var rcel = document.createElement("script");rcel.id = "rc_" + Math.floor(Math.random() * 1000);rcel.type = "text/javascript";rcel.src = "https://trends.revcontent.com/serve.js.php?w=84232&t="+rcel.id+"&c="+(new Date()).getTime()+"&width="+(window.outerWidth || document.documentElement.clientWidth)+"&referer="+referer;rcel.async = true;var rcds = document.getElementById("rcjsload_5ba490"); rcds.appendChild(rcel);})();';
      $('.view-display-id-m_entertainment_articles').find('#rcjsload_5ba490').append(rcjsload_5ba490);
    });

    AddAdCodes();
    // console.log('Load Completed');
  });
  $(document).ready(function() {
    const mq = window.matchMedia("(max-width: 768px)");
    if (mq.matches) {
      $('body').addClass('mobile');
    // Article Read It Now
      $('a.view-more-link').each(function(e){
        $(this).click(function(e) {
          e.preventDefault();
          $(this).text($(this).text() === 'View Less' ? 'Read It Now' : 'View Less');
          $(this).parent().siblings('.col-exp').toggleClass('short-text long-text');
        });
      });
    } else {
      $('body').addClass('no-mobile');
    // Article Read It Now
      $('a.view-more-link').click(function(e) {
        e.preventDefault();
        $(this).text($(this).text() === 'View Less' ? 'Read It Now' : 'View Less');
        $(this).parent().siblings('.col-exp').toggleClass('short-text long-text');
        $.scrollTo('.short-text', 'slow', {
          'offset': 0
        });
        console.log('Masonry 1 Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });

      });
    }
    //Inarticle Start
    var p_count = 0;
    $('.node-type-add-news-article .field-name-body .field-item').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle1'></div>").insertAfter($('.node-type-add-news-article .field-name-body .field-item').find('p:eq('+ half+ ')'));
    //Inarticle end
    //Inarticle1 Start
    var p_count = 0;
    $('.view-display-id-mobile_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle1'></div>").insertAfter($('.view-display-id-mobile_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle1 end
    //Inarticle2 Start
    var p_count = 0;
    $('.view-display-id-m_politics_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle2'></div>").insertAfter($('.view-display-id-m_politics_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle2 end
    //Inarticle3 Start
    var p_count = 0;
    $('.view-display-id-m_sports_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle3'></div>").insertAfter($('.view-display-id-m_sports_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle3 end
    //Inarticle4 Start
    var p_count = 0;
    $('.view-display-id-m_business_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle4'></div>").insertAfter($('.view-display-id-m_business_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle4 end
    //Inarticle5 Start
    var p_count = 0;
    $('.view-display-id-m_editorial_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle5'></div>").insertAfter($('.view-display-id-m_editorial_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle5 end
    //Inarticle6 Start
    var p_count = 0;
    $('.view-display-id-m_entertainment_articles .art-detail').find('p').each(function(e){
      p_count = p_count + 1;
    });
    half = Math.round(p_count/2);
    $("<div id='Inarticle6'></div>").insertAfter($('.view-display-id-m_entertainment_articles .art-detail').find('p:eq('+ half+ ')'));
    //Inarticle6 end

    $('.ticker-hide').css('visibility', 'visible');
    if($('.masonry-item').length[0]) {
      window.setInterval(function() {
        if( collision($('.masonry-item'), $('.footer')) == true ){
          var margin = parseInt($('.footer').css('margin-top'), 10);
          $('.footer').css('margin-top', margin+50);
        }
      }, 100);
    }
    $('.view-article-listing .masonry-item').each(function(e){
      $(this).prependTo('.view-hashtags-news-listing .view-content');
    });
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      if (btnwidth !== null) { 
        $('.mobile-right').css('width', btnwidth + 5);
        $('.mobile-right .news-addtoany a').css('width', btnwidth + 5);
      }
      else  {
        $('.mobile-right').css('width', btnwidth + 100);
        $('.mobile-right .news-addtoany a').css('width', btnwidth + 100);
      }
    // Tweeter Buttun Height
    var tweetheight = $('.mobile-share').height();
    $('.mobile .art-share a').css('height', tweetheight-10 + 'px')
    // login Layer Click
    $('.mobile-menu-close').click(function(){
      $(this).parents('.responsive-menus-simple').siblings('span.toggler').trigger('click');
    });
    // Trigre owl Caroucel Controllers
    $('a.next').click(function() {
      $(this).parents('.owl-wrapper-outer').siblings('.owl-controls').find('.owl-prev').trigger('click');
    });
    // Previous Page
    $('a.comment-back.go-back').click(function() {
      window.history.back();
    });
    // Form Resize
    var mdlheight = $(window).height();
    // console.log(mdlheight);
    $('.modal-scroll #modal-content').css('max-height', mdlheight - 100);
    $(window).resize(function() {
      var mdlheight = $(window).height();
      // console.log(mdlheight);
      $('.modal-scroll #modal-content').css('max-height', mdlheight - 50);
      // Tweeter Buttun Height
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      if (btnwidth !== null) {
        $('.mobile-right').css('width', btnwidth + 5);
        $('.mobile-right .news-addtoany a').css('width', btnwidth + 5);
      }
      else {
        $('.mobile-right').css('width', btnwidth + 100);
        $('.mobile-right .news-addtoany a').css('width', btnwidth + 100);
      }
    });
    $('#feed-node-form, #add-news-article-node-form').addClass('col-md-8 col-md-offset-2')
    $('#add-news-article-node-form .form-group').addClass('col-md-12');
    $('#edit-feeds .panel-title.fieldset-legend').text('Keyhole API Endpoint');
    $('#autocomplete-deluxe-input, #edit-field-news-hashtags-tid-value-field, #edit-field-news-hashtags-tid').each(function(e){
      $(this).on('focus keyup mouseup', function(e) {
        var text = $(this).val();
        if (text.charAt(0) != '#') {
          $(this).val('#');
        }
      });
    });
    $('.check-link').click(function(e) {
      e.preventDefault();
      var path = $('#edit-field-news-link-und-0-url');
      if (!ValidURL(path.val())) {
        alert('Please enter a valid url. E.g http://example.com.');
        if ($(this).find('i').hasClass('fa-times')) {} else if ($(this).find('i').hasClass('fa-check')) {
          $(this).find('i').removeClass('fa-check').addClass('fa-times');
        } else {
          $(this).append('<i class="fa fa-times" aria-hidden="true"></i>');
          $(this).find('.fa').css('padding-left', '10px');
        }
      } else {
        $.ajax({
          url: path,
          crossDomain: true,
          complete: function(xhr, statusText) {
            if (xhr.status == 200) {
              if ($('.check-link').find('i').hasClass('fa-check')) {} else if ($('.check-link').find('i').hasClass('fa-times')) {
                $('.check-link').find('i').removeClass('fa-times').addClass('fa-check');
              } else {
                $('.check-link').append('<i class="fa fa-check" aria-hidden="true"></i>');
                $('.check-link').find('.fa').css('padding-left', '10px');
              }
            }
          },
          error: function(xhr, statusText) {
            alert('Please enter a valid url. E.g http://example.com.');
          },
        })
      }
    });
    // Tooltip
    $('#edit-field-news-link-und-0-url').attr("data-toggle", "tooltip").attr("data-html", "true").attr("maxlength", "1024").attr("title", '- Enter the link for your article then click “Check Link”');
    // Sticky Nav
    $(function() {
      $(window).scroll(function() {
        function addStickyMenu() {
          if(!($("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").hasClass('sticky'))) {
            $("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").addClass('sticky');
          }
        }
        function removeStickyMenu() {
          if($("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").hasClass('sticky')) {
            $("body, section#block-views-hashtags-listing-popular-tags, .navigation-div, .sliding-bar .block-views .views-exposed-form").removeClass('sticky');
          }
        }
        function addStickyCat() {
          if(!($("#block-menu-menu-news-category, body:not(body.page-node-23)").hasClass('stick'))) {
            $("#block-menu-menu-news-category, body:not(body.page-node-23)").addClass('stick');
          }
        }
        function removeStickyCat() {
          if($("#block-menu-menu-news-category, body:not(body.page-node-23)").hasClass('stick')) {
            $("#block-menu-menu-news-category, body:not(body.page-node-23)").removeClass('stick');
          }
        }
        var winWidth = $(window).width();
        var winTop = $(window).scrollTop();
        if (winWidth >= 1199) {
          if (winTop >= 95) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 250) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 1023) {
          if (winTop >= 80) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 242) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 989) {
          if (winTop >= 80) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 242) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth >= 767) {
          if (winTop >= 70) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
          if (winTop >= 225) {
            addStickyCat();
          } else {
            removeStickyCat();
          }
        } else if (winWidth <=768 && winWidth >=480) {
          if (winTop >= 145) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        } else if (winWidth <=479 && winWidth >=320) {
          if (winTop >= 130) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        }else if (winWidth <=319) {
          if (winTop >= 120) {
            addStickyMenu();
          } else {
            removeStickyMenu();
          }
        }
      });
    });
    $('form.node-hash_tag_news-form').on('submit', function() {
        var path = $('#edit-field-news-link-und-0-url');
        if (!ValidURL(path.val())) {
          alert('Please enter a valid url. E.g http://example.com.');
          return;
        }
      })
      //Replace Share Text
    //palceholder
    $('#edit-field-news-link-und-0-url').attr('placeholder', $.parseHTML("&#xf0c1;")[0].data + '  ' + 'Enter the article URL here...').css({
      "font-size": "17px"
    });
    $('#edit-field-news-caption-und-0-value').attr('placeholder', 'Tag lines goes here...').css({
      "font-size": "17px"
    });
    // Close Login Form By Clicking Any Where
    Drupal.behaviors.ctools_backdrop_close = {
        attach: function(context, settings) {
          $('#modalBackdrop').once('ctools_backdrop_close', function() {
            $(this).click(function() {
              Drupal.CTools.Modal.dismiss();
            });
          });
        }
      }
      // $('#edit-field-news-markup').parent().next().find('a').addClass('checklink');
    $('a.a2a_dd.addtoany_share_save').addClass('fa fa-share-alt');
    $('.match-height').matchHeight();
    $('.menu-image').after().append('<p class="grad-back"></p>');
    $('#block-block-16').find('.recentcomment').attr('id', 'style');
    // $('a.sidebar-arrow').click(function(e) {
    //   $('a.sidebar-arrow i.fa').toggleClass('fa-chevron-left');
    //   var parent = $(this).closest('aside');
    //   parent.toggleClass("open");
    //   var parent1 = $(this).parents().find('.hashtag-list');
    //   e.preventDefault();
    // });
    $('a.sidebar-arrow').click(function(){
      $(this).parents('.header-top').siblings('.responsive-menus').find('.toggler').trigger('click');
    })
    $('div#rm-removed').on('click', function(e) {
      $(this).siblings('.toggler').trigger('click');
    });
    $("div#rm-removed").find('*').click(function(e) {
      e.stopPropagation();
    });
    // $('span.toggler').click(function(){
    //   $(this).find('a.sidebar-arrow').toggle();
    // })
    placeholders();

    function placeholders() {
      $('#edit-field-news-hashtags #autocomplete-deluxe-input').each(function() {
        $(this).attr('placeholder', '# Add new hashtag here!');
      });
      $('.sliding-bar .views-exposed-form #autocomplete-deluxe-input, #edit-field-news-hashtags-tid, .autocomplete-deluxe-form').each(function() {
        $(this).attr('placeholder', '# Search');
      });
    }
    // the hashtag news text pop up
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    if (btn) {
      btn.onclick = function() {
        modal.style.display = "block";
      }
    }
    if (span) {
      span.onclick = function() {
        modal.style.display = "none";
      }
    }
    if (window) {
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
    }
    // end pop up
    var setHeight = $(window).height();
    $('.region.region-mobile-menu').css('height', setHeight + 'px');
    $("div.modal-forms-modal-content .popups-container").css('height', setHeight);
    $('#block-block-11 i').click(function(e) {
      $('#block-block-11').fadeOut();
    });
    $('.form-item-edit-field-news-category-tid-12 a').trigger('click');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-12 a').prepend('<i class="fa fa-clock-o"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-11 a').prepend('<i class="fa fa-star"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-13 a').prepend('<i class="fa fa-snapchat-ghost"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-14 a').prepend('<i class="fa fa-play"></i>');
    $('form#views-exposed-form-hashtags-news-listing-block div#edit-field-news-category-tid-wrapper .form-item-edit-field-news-category-tid-15 a').prepend('<i class="fa fa-user"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(1) a').prepend('<i class="fa fa-home"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(2) a').prepend('<i class="fa fa-commenting"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(3) a').prepend('<i class="fa fa-car"></i>');
    $('.navbar-menu-bottom .block.block-menu ul.menu.nav li.leaf:nth-child(4) a').prepend('<i class="fa fa-cog"></i>');
    $('#edit-field-news-category-und option[value="_none"]').text('Select Category');
    $('.node.node-hash-tag-news.view-mode-full .ds-left span.a2a_kit.a2a_kit_size_32.a2a_target.addtoany_list a').addClass('fa fa-share-alt');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-node-link.field-type-ds a').addClass('fa fa-eye');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-field-news-comments .field-items .field-item.even').addClass('fa fa-comment');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-retweets.field-type-number-decimal .field-item.even').addClass('fa fa-retweet');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-favorites.field-type-number-decimal .field-item.even').addClass('fa fa-heart');
    $('form#hash-tag-news-node-form input#edit-field-news-url-und-0-url').attr("placeholder", $.parseHTML("&#xf0c1;")[0].data + " Enter the article URL here...").css('font-size', '17px');
    $('input#edit-field-news-source-und').attr("placeholder", $.parseHTML("&#xf0e7;")[0].data + " Website Name (Source)").css('font-size', '17px');
    $('textarea#edit-field-news-body-und-0-value').attr("placeholder", "Tag line goes here").css('font-size', '17px');
    $('form#hash-tag-news-node-form .autocomplete-deluxe-container.autocomplete-deluxe-multiple').prepend('<p>ASSIGN HASHTAG <span>(add as many as you like!)</spane></p>');
    $('footer.footer .form-border input#edit-mergevars-email').attr("placeholder", "Your email adress").css('font-size', '17px');
    // console.log('Document Ready');
    $('.row-ad:last .banner-ad').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
      } else {}
    });
    $('.mobile.not-front .row-ad:last, .no-mobile .row-ad:last').prev().find('.tweet-listing').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
        console.log('Masonry Oembed Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
        // $('.pager.pager-load-more a').trigger('click');
      } else {}
    });
  });
  $(document).ajaxComplete(function(e) {
    $('span.close').click(function(){
      $(this).parents('.modal').trigger('click');
    });
    // Form Resize
    var mdlheight = $(window).height();
    // console.log(mdlheight);
    $('.modal-scroll #modal-content').css('max-height', mdlheight - 100);
    $(window).resize(function() {
      var mdlheight = $(window).height();
      // console.log(mdlheight);
      $('.modal-scroll #modal-content').css('max-height', mdlheight - 50);
      // match width of buttons
      var btnwidth = $('.owl-prev').width();
      $('.mobile-right .news-viewnews').css('width', '100%');
    });
    var setHeight = $(window).height();
    $("div.modal-forms-modal-content .popups-container").css('height', setHeight);
    $(".login-footer").css('top', setHeight - 45);
    $(".register-footer h5:last-child").css('top', setHeight - 30);
    // Add Icons
    $('.node.node-hash-tag-news.view-mode-full .ds-left span.a2a_kit.a2a_kit_size_32.a2a_target.addtoany_list a').addClass('fa fa-share-alt');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-node-link.field-type-ds a').addClass('fa fa-eye');
    $('.node.node-hash-tag-news.view-mode-full .ds-left .field.field-name-field-news-comments .field-items .field-item.even').addClass('fa fa-comment');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-retweets.field-type-number-decimal .field-item.even').addClass('fa fa-retweet');
    $('.node.node-hash-tag-news.view-mode-full .ds-right .field.field-name-field-news-favorites.field-type-number-decimal .field-item.even').addClass('fa fa-heart');
    // Signin / Signup Place Holders
    $('form#user-login .form-item-name input#edit-name').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('.form-item-name input#edit-name--2').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('form#user-login .form-item-pass input#edit-pass').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('.form-item-pass input#edit-pass--2').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('form#user-register-form .form-item-name input#edit-name--2').attr("placeholder", $.parseHTML(" ")[0].data + "User Name").css('font-size', '17px');
    $('form#user-register-form .form-item-name input#edit-name--3').attr("placeholder", $.parseHTML(" ")[0].data + "User Name").css('font-size', '17px');
    $('form#user-register-form .form-item-mail input#edit-mail--2').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('form#user-register-form .form-item-pass-pass1 input#edit-pass-pass1--2').attr("placeholder", $.parseHTML(" ")[0].data + "Password").css('font-size', '17px');
    $('form#user-register-form .form-item-pass-pass2 input#edit-pass-pass2--2').attr("placeholder", $.parseHTML(" ")[0].data + "Re-Type Password").css('font-size', '17px');
    $('form#user-pass .form-item-name input#edit-name--3').attr("placeholder", $.parseHTML(" ")[0].data + "Email").css('font-size', '17px');
    $('div.modal-forms-modal-content .popups-close').addClass('fa fa-times').text('');
    $('.register-footer').click(function() {
      $(this).hide();
      $('.modal-default, .backdrop-default').hide();
    });
    $('.backdrop-default').click(function() {
      $(this).hide();
      $('.modal-default, .backdrop-default').hide();
    });
    $('.row-ad:last .banner-ad').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
      } else {}
    });
    $('.mobile.not-front .row-ad:last, .no-mobile .row-ad:last').prev().find('.tweet-listing').bind('DOMNodeInserted DOMNodeRemoved DOMSubtreeModified', function(event) {
      if (event.type == 'DOMNodeInserted') {
      } else if (event.type == 'DOMSubtreeModified') {
      } else if (event.type == 'DOMNodeRemoved') {
        console.log('Masonry Ajax Oembed Called');
        $(this).parents('.view-content.masonry-processed').masonry({
          itemSelector: '.masonry-item',
          columnWidth: '.col-md-3',
          percentPosition: true
        });

      } else {}
    });
    // console.log('Ajax Completed');
  });
  var AddAdCodes = function AddAdCodes(e) {
    var MobileTopBanner = document.createElement('script');
    MobileTopBanner.type = 'text/javascript';
    MobileTopBanner.async = true;
    MobileTopBanner.innerHTML="googletag.cmd.push(function(){googletag.display('MobileTopBanner');});";
    $('#MobileTopBanner').append(MobileTopBanner);
    var MobileBottomMPU = document.createElement('script');
    MobileBottomMPU.type = 'text/javascript';
    MobileBottomMPU.async = true;
    MobileBottomMPU.innerHTML="googletag.cmd.push(function(){googletag.display('MobileBottomMPU');});";
    $('#MobileBottomMPU').append(MobileBottomMPU);
    var Slider1 = document.createElement('script');
    Slider1.type = 'text/javascript';
    Slider1.async = true;
    Slider1.innerHTML="googletag.cmd.push(function(){googletag.display('Slider1');});";
    $('#Slider1').append(Slider1);
    var Slider2 = document.createElement('script');
    Slider2.type = 'text/javascript';
    Slider2.async = true;
    Slider2.innerHTML="googletag.cmd.push(function(){googletag.display('Slider2');});";
    $('#Slider2').append(Slider2);
    var Slider3 = document.createElement('script');
    Slider3.type = 'text/javascript';
    Slider3.async = true;
    Slider3.innerHTML="googletag.cmd.push(function(){googletag.display('Slider3');});";
    $('#Slider3').append(Slider3);
    var Slider4 = document.createElement('script');
    Slider4.type = 'text/javascript';
    Slider4.async = true;
    Slider4.innerHTML="googletag.cmd.push(function(){googletag.display('Slider4');});";
    $('#Slider4').append(Slider4);
    var Slider5 = document.createElement('script');
    Slider5.type = 'text/javascript';
    Slider5.async = true;
    Slider5.innerHTML="googletag.cmd.push(function(){googletag.display('Slider5');});";
    $('#Slider5').append(Slider5);
  }
  var ValidURL = function ValidURL(str) {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if (!regex.test(str)) {
      // alert("Please enter valid URL.");
      return false;
    } else {
      return true;
    }
  }

  var collision = function collision($div1, $div2) {
    var x1 = $div1.offset().left;
    var y1 = $div1.offset().top;
    var

    h1 = $div1.outerHeight(true);
    var w1 = $div1.outerWidth(true);
    var b1 = y1 + h1;
    var r1 = x1 + w1;
    var x2 = $div2.offset().left;
    var w2 = $div2.outerWidth(true);
    var y2 = $div2.offset().top;
    var h2 = $div2.outerHeight(true);
    var b2 = y2 + h2;
    var r2 = x2 + w2;

    if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
    return true;
  }
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };
})(jQuery);
